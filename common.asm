macro CHANGE_BORDER color
	push	bc
	ld	a,(#0006)
	inc	a
	ld	c,a
	ld	a,color
	di
	out	(c),a
	and	#c0
	out	(#aa),a	
	ld	a,87h	;register 7 + 80h
	out	(c),a
	pop	bc
endmacro

RDVDP  EQU     0006H
WRVDP  EQU     0007H
RDSLT	equ	#000c
WRSLT	equ	#0014
DCOMPR	equ	#0020
ENASLT	equ	#0024
BASVER	equ	#002d
DISSCR	equ	#0041
ENASCR	equ	#0044
WRTVDP	equ	#0047
SETWRT	equ	#0053
FILVRM	equ	#0056
LDIRVM	equ	#005c
CHGMOD	equ	#005f
CHGET	equ	#009f
CLS	equ	#00c3
RSLREG	equ	#0138
SNSMAT	equ	#0141
CHGCPU	equ	#0180
GETCPU	equ	#0183
BASRUN  equ	#73ac
BDOS            equ     #f37d

DSKSLT	equ	#f348
HIMEM	equ	#f380
CLIKSW	equ	#f3db
FORCLR	equ	#f3e9
BAKCLR	equ	#f3ea
BDRCLR	equ	#f3eb

MEMSIZ  equ	#f672
STKTOP  equ #f674
TXTTAB  equ #f676
FRETOP  equ #f69b
VARTAB  equ #f6c2
BOTTOM  equ #fc48
CAPST   equ #fcab
H.KEYI	equ	#fd9a
H.TIMI	equ	#fd9f
H.STKE  equ #feda
H.ERRO  equ #ffb1

EXPTBL	equ	#fcc1
SLTTBL	equ	#fcc5
H.PHYD          equ     #FFA7