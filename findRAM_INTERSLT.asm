    ;================================================
    ;
    ; findRAM INTERSLOT VERSION
    ;
    ; Copyleft 2020 by Jose Angel Morente for the MSXWIKI.ORG project
    ;
    ; finds an available RAM slot for a given page
    ;
    ; input parameters
    ;
    ; H = MSB of page being searched
    ;
    ; output parameters
    ;   E = slot ID (in BIOS format)
    ;   Cy = set if RAM found, otherwise unset
    ;
    ; Usage:
    ;
    ;   This routine recurres all possible slots and subslots (if expanded) for
    ;   a given page (#0000, #4000, #8000) and returns a slot ID if it is
    ;   writable.
    ;   No other slot management operation is required (no #a8 or #ffff switching).
    ;   It can find the RAM slot for the same page the routine is being run from (even
    ;   if execution is happening from a ROM cartridge).
    ;
    ;   The value returned on E register may be safely used with other slot-related
    ;   BIOS routines.
    ;
    ;   Limits:
    ;
    ;   * It cannot be used to search Page 3 (#c000-#ffff) since RDSLT and WRSLT
    ;     actively use system variables during the slot switching process. In any case,
    ;     RAM slot is selected by default during the boot process for page 3.
    ;   * Since it is a BIOS-based routine, the BIOS must be active at the slot pointed by
    ;     EXPTBL address.
    ;   * Since it is a BIOS-based routine, it must reside in other page than Page 0.
    ;   * The writing test is performed by modifying random addresses within the first #100
    ;     bytes of the page being searched. Because of this, the routine should not run from
    ;     the #xx00 - #xxFF range in order to prevent an unexpected behaviour.
    

findRAMpgX:
    di

    ld  b,4    ;countdown for main slot
main_loop:

    ld  a,b
    dec a
    call    checkExp    ;check if expanded
    push    bc
    call    c,searchExp
    pop bc
    ret c
    ;check non-expanded slot
    ld  a,b
    dec a
    ld  e,a
    push    de
    call    checkWrt
    pop de
    ret z
    djnz    main_loop
    scf
    ccf
    ret

checkExp:
    exx
    ld  c,a
    ld  b,0
    ld  hl,EXPTBL
    add hl,bc
    ld  a,(hl)
    rlca
    exx
    ret
checkWrt:
    push    bc
    ld  l,a ;random value to avoid #xx00 addresses and preserves input slots in just one operation
    call    RDSLT
    ld  d,a
    inc a
    ld  e,a ;value to be written
    ld  a,l ;restore slot number
    call    WRSLT
    ld  a,l
    push    de
    call    RDSLT
    pop     de
    cp  e
    scf
    pop bc
    ret
searchExp:
    ld  d,b
    dec d
    ld  b,4
exp_loop:    
    ld  a,b
    dec a
    sla a
    sla a
    or  d
    set 7,a ;flag as expanded
    ld  e,a ;save slot setup for the return
    ;H is supposed to keep page MSB address
    push    de
    call    checkWrt
    pop     de
    ret z
    djnz    exp_loop
    scf
    ccf ;not Cy, RAM not found
    ret
    