Const SECTOR_SIZE=512

Dim fso,fsoO
Set fso = WScript.CreateObject("Scripting.Filesystemobject")

Set fsoO = fso.OpenTextFile("extract.bat", 8, True)


Dim oF,iCF

Set oCF = fso.OpenTextFile(WScript.Arguments(0))	'text file


While Not oCF.AtEndOfStream 
	Dim dummy,values
	dummy = oCF.ReadLine
	values = Split(dummy,Chr(9))
	sectorCount = CInt("&H"+values(0) ) * SECTOR_SIZE
	sectorOffset = CInt("&H"+values(1) ) * SECTOR_SIZE
	opllPatch = Trim(values(2))
	fsoO.WriteLine "dd bs=1 if=" & WScript.Arguments(1) & " of=" & Left(WScript.Arguments(1),InStr(WScript.Arguments(1),".")-1) + "_" + LCase(Right("00"+values(1),3)) _
		& "_"& opllPatch & ".dat" & " skip=" & sectorOffset & " count=" & sectorCount
Wend
