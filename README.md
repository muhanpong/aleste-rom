# ALESTE 2 MEGAROM PATCH #

Aleste 2 is a great game by Compile for the MSX2 (and upper) series of home computers. It was a sequel to "Aleste" and a precursor to "M.U.S.H.A." for the Megadrive.

The original MSX2 game had some drawbacks that had been fixed with this patch.

* Conversion to a 738KB ROM. Now, the game can be played uninterruptedly.
* The difficulty level was wrong due to an uninitialized variable.
* Many slowdowns occur due to overload on the Z80 CPU. The game takes advantage of the R800 CPU if played on a MSXTurboR to fix this problem.
* FM routines do direct illegal I/O access, preventing the right playback on fast CPUs. It has been fixed so the music now works fine on both Z80 and R800.
* Fixed a bug that caused corrupted graphics in R800 mode.


### Software needed to use this patch ###

* [SjAsm](https://github.com/Konamiman/Sjasm)
* Any Windows OS (32bit/64bit) for some generation scripts (that can be easily converted to Python, Perl, Bash, etc.)
* DD (https://en.wikipedia.org/wiki/Dd_(Unix))
* [Bitbuster compressor by Team Bomba](https://www.teambomba.net/bombaman/downloadd26a.html)
* Any IPS patcher [Lunar IPS](http://www.romhacking.net/utilities/240) will do the trick


### Building instructions ###

This fix has been designed to be applied to a digital copy of the original floppies. Please, get a disk image file for each Aleste 2 disk. If you want to dump your own original Aleste 2, use any tool with disk imaging capabilities: DD for Linux/Mac/Windows, Disk Utility for Mac, Winimage for Windows, etc.
Disk images must be standard 737280 bytes sized files (no headers or physical sector information).

The file names must be:

* Demo Disk => aleste21.dsk
* Game Disk A => aleste22.dsk
* Game Disk B => aleste23.dsk

Please, use Lunar IPS (or any other IPS compatible tool) and patch the disks by using IPS patches in /patch directory.

Execute the MAKE.BAT batch file on Windows. Be sure that DD.exe and pack.exe are accessible via PATH or the local directory.

