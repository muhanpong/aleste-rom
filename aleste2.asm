;================================================================================
/*                  
                Aleste 2 MegaROM Patch
                 by MSXWiki.org 2021

                Please, use SjASM to assemble this source code

                LICENSE:
                do WHAT YOU WANT with this piece of code. Sell it, send it,
                burn it, stole it, cook it or just enjoy it.

                Please read LICENSE.txt for more licensing details.

                @TODO

                -Use    #F23D for actual current DMA storage
                
                */
;================================================================================

                include "common.asm"

PG1_SLT         equ     #f342
PG3_SLT         equ     #f344

SKILL           equ     #c878

OPLLREG         equ     #f000
OPLLDAT         equ     OPLLREG + 3

stack           equ     #dfff
LAUNCH          equ     #c106


                output  "aleste2.rom"

                defpage 0,#4000,8192        ;main loader
                defpage 1,0,1048576-8192  ;compressed data

                page    0

                db      "AB"
                dw      init
                ds      12

                db      "JaM Aleste 2 Loader v2.0/R800 Support  "

init:
                di      
                ld      sp,#dfff

                call    cleanRAM                        
                call    inst_e000
                call    checkCheat
                call    checkChgCpu
                call    save_SYS_slot   ;RAM not initialized by DOS, need to know RAM page 3 slot
                call    save_CRT_slot
                call    save_RAM1_slot
                call    set_RAM2
                call    inst_BDOS_patch 
                call    inst_opllPatch

                xor     a
                ld      (DISCNO),a      ;start disc 1

                ld      a,#80
                ld      (CURCPU),a

                call    ENAZ80
                
                jp      LOADER


inst_e000:
                ld      hl,e000_data
                ld      de,#e000 
                ld      bc,e000_end-rel_e000
                ldir
                ret

patch:
    ;------------MAIN BDOS EMULATION ROUTINE---------------
    ;
    ;DTA functions should be forwarded to the real diskrom, although the DE pointer must be save_BDOS_hook
    ;
    ;-----------------------------------------------------

                ld      a,c
                cp      #1a ;set DTA
                jr      z,handle_1a
                cp      #1b
                jr      z,handle_1b
                cp      #2f ;read sectors
                jr      z,handle_2f

noBDOS:
                scf
                ccf ;disable Cy flag for no error
                ret
handle_1a:
                ld      (DTA),de
                ret
handle_1b:
                ;pop     af
        ;return typical disk structure data
                ld      a,2 ;usually 2 sectors percluster in FAT12 disks
                ld      bc,512  ;usually 512 bytes per sector
                ld      de,640  ;number of clusters in disk
                ld      hl,0    ;free clusters in disk
                ld      ix,0    ;pointer to DPB
                ld      iy,#f197    ;pointer to first FAT sector

                ret
handle_2f:
        ;DE = sector number
        ;H = number of sectors, can be discarded, size it is implicit in compressed data
        ;(DISCNO) = number of disc

                ;accelerate loading, if possible
                di
                call    waitVDP ;some VDP commands might by still ongoing, the game does NOT check it and relies it on the slowness of the floppy ¬¬
                
                ld      a,(CHGCPUAVAL)
                or      a
                ld      a,#82
                call    nz,CHGCPU       ;don't update CURCPU

                ld      a,(DISCNO)
                rlca    ;A = A * 2
                ld      b,0
                ld      c,a
                ld      hl,index_tbl
                add     hl,bc
                call    ldhlhl; ld      hl,(hl)      HL = sector lookup table 
                
                ;input DE = sector number for LUT searching
                ;input HL = pointer to LUT
                ;
                ;output HL = offset
                ;output A = page number
                ;output C = OPLL patch flag
                ;
                call    searchSecIdx
                push    bc      ;save C register (OPLL patch flag)

                ld      de,(DTA)        ;destination of copied data
                ld      bc,#4000    ;start of PG1
                add     hl,bc   ;absolute origin of compressed data

                call    handle_2fb  ;call the actual LDIR from the top routine, since both mapper 0/1 page will be switched
                
                pop     bc
                ld      a,c
                cp      1       ;patch OPLL port access
                exx
                call    z,patch_opll
                exx
                cp      2       ;patch OPLL vector
                call    z,patch_opll_vec
     
                ld      a,(CURCPU)
                cp      #80
                call    z,ENAZ80

                xor     a   ;no error

                ret
searchSecIdx:
                ;Input:
                ;DE = sector to compare
                ;HL = index table
                ;
                ;Output:
                ;A = page number
                ;C = OPLL patch flag
                ;HL = origin address

                ld      b,h
                ld      c,l     ;save HL
searchSecIdx_loop:
                call    ldhlhl; ld hl,(hl)
                
                or      a       ;reset Cy flag
                sbc     hl,de
                jr      z,secIdxFound      ;found
                inc     bc
                inc     bc
                inc     bc
                inc     bc
                inc     bc
                inc     bc
                ld      h,b
                ld      l,c

                jr      searchSecIdx_loop
secIdxFound:
                inc     bc
                inc     bc
                ld      a,(bc)
                ex      af,af'
                inc     bc
                ld      a,(bc)
                ld      l,a
                inc     bc
                ld      a,(bc)
                ld      h,a
                inc     bc
                ld      a,(bc)  ;OPLL patch flag
                ld      c,a
                ex      af,af'
                ret

patch_opll:
                ld      a,(CURCPU)
                cp      #80
                ret     z       ;R800 not being used

                
                ;patch REG write (rst #18 = #df)

                ld      c,#df
                ld      de,opll_regpos
                ld      b,15
                call    patch_pos

                ;patch DATA write (rst #20 = #e7)
                ld      c,#e7
                ld      de,opll_datpos
                ld      b,16
                ;call    patch_pos                


patch_pos:
                push    de
                pop     hl
                ld      a,(hl)
                inc     hl
                ld      h,(hl)
                ld      l,a
                ld      (hl),c  ;7C or 7D
                inc     hl
                ld      (hl),0  ;NOP
                inc     de
                inc     de
                djnz    patch_pos
                ret                                        

patch_opll_vec:
                ;d8aa = subslot (00)
                ;d8ab = slot (ff)
                ;build the slot in BIOS format
                ld      a,(#d8ab)
                and     #03     ;slot number
                
                ld      hl,EXPTBL
                ld      b,0
                ld      c,a
                add     hl,bc
                inc     hl
                inc     hl
                inc     hl
                inc     hl
                ld      a,(hl)
                and     #80
                or      c
                ld      b,a
                ld      a,(#d8aa)
                and     #03
                rlca
                rlca
                or      b


                ;   Input:
                ;       HL = origin address
                ;       DE = destination address
                ;       BC = length
                ;       A = origin slot
                ;       A'= destination slot
                ld      hl,opll_vecdat
                ld      de,#18
                ld      bc,8+3
                ex      af,af'
                ld      a,(CRT_SLT)
                jp      intslt_LDIR
                
                
opll_regpos:
                dw      #8992,#89A7,#89C4,#89DE,#89F6,#9158,#9194,#91C6,#91EF,#91FC,#9317,#9371,#937F,#9390,#93B2
opll_datpos:
                dw      #8996,#89aa,#89cc,#89E1,#89F9,#915B,#9197,#91c9,#915B,#9201,#920A,#931A,#9374,#9383,#9394,#93B5
        
opll_vecdat:
                db      #c3
                dw      OPLLREG
                db      #00,#00,#00,#00,#00

                db      #c3
                dw      OPLLDAT

cleanRAM:
                xor     a
                ld      hl,DTA
                ld      (hl),a
                ld      de,DTA+1
                ld      bc,#1000+#10
                ldir
                ret
                
inst_BDOS_patch:

                ld      hl,cusbdos_data
                ld      de,CUS_BDOS
                ld      bc,cusbdos_end-rel_cusbdos
                ldir
                ld      hl,CUS_BDOS+1
                ld      a,(CRT_SLT)
                ld      (hl),a  ;CRT slot for RST #30 usage


                ld      hl,BDOS
                ld      (hl),#c3    ;JP
                ld      de,CUS_BDOS
                inc     hl
                jp      ld_hl_de


inst_opllPatch:
                ld      hl,opll_data
                ld      de,OPLLREG
                ld      bc,opll_end-rel_opll
                ldir
                ret

checkCheat:
                ld      a,3     ;'J'
                call    SNSMAT
                cp      #7f     ;bit     7,a
                ret     nz
                ld      a,2
                call    SNSMAT
                cp      #bf     ;bit     6,a
                ret     nz
                ld      a,4
                call    SNSMAT
                cp      #fb     ;bit     2,a
                ret     nz
                ld      a,1                
                ld      (CHEAT),a
                ret
checkChgCpu:
                ld      a,(#2D)
                cp      3       ;check if TR A >= 3             ;nc = great or equal to
                jr      c,checkChgCpu_no
checkChgCpu_upd:                
                ld      (CHGCPUAVAL),a
                ret     
checkChgCpu_no:
                xor     a
                jr      checkChgCpu_upd

save_SYS_slot:
                call    getSYSslot
                ld      (PG3_SLT),a
                ret
save_CRT_slot:
                call    getCRTslot
                ld      (CRT_SLT),a
                ret

save_RAM1_slot:
                ld      h,#40
                call    findRAMpgX
                jr      nc,no_ram
                ld      a,e
                ld      (PG1_SLT),a
                ret
set_RAM2:
                ld      h,#80
                call    findRAMpgX
                jr      nc,no_ram       
                ret
no_ram:
                include "memERROR.asm"

ld_hl_de:
                ld      (hl),e
                inc     hl
                ld      (hl),d
                inc     hl
                ret
ldhlhl:
                ld      a,(hl)
                inc     hl
                ld      h,(hl)
                ld      l,a
                ret

index_tbl:
                dw      romIndex0
                dw      romIndex1
                dw      romIndex2

                include "index0.asm"
                include "index1.asm"
                include "index2.asm"

                include "findRAM_INTERSLT.asm"
                include "intslt_LDIR.asm"
                include "getCRTSYSslot.asm"
                include "waitVDP.asm"
                
;========================================================================================
;BEGIN RELOCATABLE MODULES
;========================================================================================

;====================================================================
;Custom BDOS LOADER
;
cusbdos_data:
                org     CUS_BDOS
rel_cusbdos:
                rst     #30
                db      0       ;CRT slot
                dw      patch
                ret
cusbdos_end:
                org     cusbdos_data + cusbdos_end - rel_cusbdos
;====================================================================


;====================================================================
;safe OPLL writing
;====================================================================
opll_data:
                org OPLLREG
rel_opll:
        module
                db      #c3     ;JP op-code, to prevent jump optimization
                dw      opllregw
                db      #c3     ;JP op-code, to prevent jump optimization
                dw      oplldatw
opllregw:

                out     (#7c),a
                push    bc
                push    af
                ld      b,3
                call    opllwait
                pop     af
                pop     bc
                ret
oplldatw:
                out     (#7d),a
                push    bc
                push    af
                ld      b,13
                call    opllwait
                pop     af
                pop     bc
                ret
        
opllwait:
                in  a,(#E6)
                ld  c,a
opllwait_loop:        
                in  a,(#E6)
                sub c
                cp  b
                jr  c,opllwait_loop
                ret
        endmodule                
opll_end:
                org     opll_data + opll_end - rel_opll

;====================================================================

;======================================================================================================
;#E000 BLOCK ROUTINES
;
;
e000_data:
                org     #e000

rel_e000:
        module
DTA             dw      0
DISCNO          db      0
CRT_SLT         db      0       
CHGCPUAVAL      db      0
CURCPU          db      0
CUS_BDOS        ds      5       ;rst #30 + slot + jump addr
CHEAT           db      0

E000_VAR_END:
                ds      #e100-E000_VAR_END

LOADER_V:
                db      #c3     ;skip optimizing process
                dw      LOADER
HANDLE_2FB_V:
                db      #c3
                dw      handle_2fb
R800A1_V:
                db      #c3
                dw      r800a1
R800A5_V:
                db      #c3
                dw      r800a5
DEMO_2:
                db      #c3
                dw      demo2
DEMO_3:
                db      #c3
                dw      demo3
MENU_1:
                db      #c3
                dw      menu1
TITLE_1:
                db      #c3
                dw      title1                
CHEAT_MODE:
                db      #c3
                dw      cheatMode                
        ;-------------------------------------
        ;function LOADER
        ;-------------------------------------
LOADER:
                ld      a,(PG1_SLT)
                ld      h,#40
                call    ENASLT

                jp    load_BOOT

                ;ld      sp,stack
                ;jp      LAUNCH
load_BOOT:
                ld      hl,#f323
                ld      (#d00e),hl
                ld      (#d017),de
                ld      (#c07a),de
                ld      (hl),#77
                inc     hl
                ld      (hl),#c0        ;Error handling, remove
                ld      sp,(#f674)
                ld      de,#d030
                ld      c,#1a
                call    BDOS

                ld      c,#2f
                ld      h,3
                ld      de,#000e
                call    BDOS
                ld      a,#ff
                ld      (#d000),a
                scf
                ld      hl,#c079
                call    #d030
                ld      a,#1
                ld      (#d01d),a
                ld      hl,#0011
                ld      de,#c100
                ld      b,2
                sub     a
                call    #d033
                xor     a
                ld      (SKILL),a       ;fix difficulty bug
                
                jp      LAUNCH

        ;-------------------------------------
        ;#2F high hook module
        ;-------------------------------------
                ; Input
                ; A = page number
                ; C = 
handle_2fb:               
                inc     a   ;first page containing data is 1
                ld      (#6000),a
                inc     a
                ld      (#6800),a

                call    depack

                xor     a
                ld      (#6000),a   ;restore ROM routines
                ret
                include "depack.asm"

        ;-------------------------------------
        ;GAMEPLAY AREAS launch high hook module
        ;-------------------------------------
r800a1:
                call    #60ce
                call    ENA800
                jp      #4522                                        
r800a5:
                call    ENA800
                ld      hl,#0013
                ret

        ;-------------------------------------
        ;CPU change routines
        ;-------------------------------------

ENA800:
                di
                call    chkCpu
                ret     z;      no R800 available
                ld      a,#82   ;Turbo-mode, no DRAM for compatibility with some MSX2+ machines
                ld      (CURCPU),a
                jp      CHGCPU
ENAZ80:
                di
                call    chkCpu
                ret     z;
                ld      a,#80
                ld      (CURCPU),a
                jp      CHGCPU
chkCpu:
                ld      a,(CHGCPUAVAL)
                or      a
                ret     ; Z = no R800 available

        
        ;-------------------------------------
        ;Intermediate Demo
        ;-------------------------------------                

demo2:
                call    ENAZ80
                ld      hl,#0013
                jp      #4892

        ;-------------------------------------
        ;Ending Demo
        ;-------------------------------------                
demo3:
                call    ENAZ80
                ret
menu1:
                call    ENAZ80
                call    #5f09
                ret
title1:
                call    ENAZ80
                ret                

cheatMode:
                push    bc
                ld      b,a
                ld      a,(CHEAT)
                or      a
                jr      nz,noDecLive
                ld      a,b
                ld      (#c840),a
noDecLive:                
                pop     bc
                ret                
        endmodule
e000_end:
                org     e000_data + e000_end - rel_e000

;======================================================================================================
pg0_end:
                ds      #4000 + (8192 - e000_data - (e000_end-rel_e000)),#ff

                page    1
                include "romData.asm"
enddata:
                ds      (1048576-8192-enddata),#ff
                end
