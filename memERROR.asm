

    ld  a,1
    ld  (BAKCLR),a
    ld  a,12
    ld  (FORCLR),a
    ld  a,1
    call    CHGMOD
    ld  hl,txtNotEnoughMemory
    ld  de,#1800
    ld  bc,txtNotEnoughMemory.e-txtNotEnoughMemory
    call    LDIRVM

    di
    ld  h,0
    ld  l,1
error_loop:    

    ld  b,h
error_loop_inner2:        
    CHANGE_BORDER   10
    djnz    error_loop_inner2    
    ld  a,h
    neg
    and #f
    ld  b,a
error_loop_inner:        
    CHANGE_BORDER   4
    djnz    error_loop_inner
    ld  a,h
    add l
    ld  h,a
    and a
    jr  z,reverse
    cp  #f
    jr  z,reverse

    jr  error_loop

reverse:
    ld  a,l
    neg
    ld  l,a
    jr  error_loop

txtNotEnoughMemory:
    db "ERROR: NOT ENOUGH MEMORY!!"
txtNotEnoughMemory.e